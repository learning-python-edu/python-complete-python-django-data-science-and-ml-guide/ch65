# Chapter 65: Introduction to the Django web framework

## Create empty Python project

Create Python project in _PyCharm_ with _Pipenv_ selecte as virtual environment manager.

## Install Django

In the project directory execute following command:
```shell
pipenv install Django==4.2.2
```

__Note:__ latest Django version is 5.0.3 (at 2024-03-21).
Will do same examples with latest version later.

