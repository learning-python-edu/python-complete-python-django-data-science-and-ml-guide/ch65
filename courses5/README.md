# Chapter 65: Introduction to the Django web framework

Sample for Django version 5.0.3 (latest at 2024-03-21).

## Create empty Python project
Create Python project in _PyCharm_ with _Pipenv_ selected as virtual environment manager.

## Install Django

In the project directory execute following command:
```shell
pipenv install Django==5.0.3
```
